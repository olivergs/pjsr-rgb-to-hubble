var CHANNEL_NAMES = {
   0: "R",
   1: "G",
   2: "B"
}

function do_scnr(view) {
   var P = new SCNR();
   P.executeOn(view);
   return view;
}


function do_pixel_math(view, expression) {
   var P = new PixelMath();
   P.expression = expression
   P.executeOn(view);
   return view;
}


function do_multiexpression_pixel_math(view, r_expr, g_expr, b_expr, a_expr) {
   var P = new PixelMath();
   P.useSingleExpression = false;
   P.expression0 = r_expr || "";
   P.expression1 = g_expr || "";
   P.expression2 = b_expr || "";
   P.expression3 = a_expr || "";
   P.executeOn(view);
   return view;
}


function do_curves_transform(view, amounts) {
   var P = new CurvesTransformation();

   for(elem in amounts) {
      P[elem] = [ // x, y
         [0.00000, 0.00000],
         [(1 - amounts[elem]), 1.00000],
         [1.00000, 1.00000]
      ];
      P[elem + "t"] = CurvesTransformation.prototype.Linear;
   }

   // Perform the transformation
   P.executeOn(view);
}




function extract_channel(image, channel) {
   var newImg = new Image();
   image.selectedChannel = channel;
   newImg.assign(image);
   return newImg;
}


function extract_rgb_channels(view) {
   var views = [];
   for (var channel = 0; channel < 3; channel++) {
      var img = extract_channel(view.image, channel);
      var newView = create_window_for_image(img, CHANNEL_NAMES[channel], true);
      views.push(newView);
   }
   return views;
}




function create_window_for_image(image, id, show) {
   var newWin = new ImageWindow(image.width, image.height, image.numberOfChannels);

   if (id) newWin.mainView.id = id;
   if (show) newWin.show();

   image.firstSelectedChannel = 0;
   image.lastSelectedChannel = image.numberOfChannels - 1;

   newWin.mainView.beginProcess();
   newWin.mainView.image.assign(image);
   newWin.mainView.endProcess();
   return newWin.mainView;
}
