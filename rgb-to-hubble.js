/* PURPOSE
 *
 * Automatic hubble palette simulation for RGB images captured using dual narrowband filter
 */

   #feature-id    StarlightHunter.com > Dual NB to Hubble palette
   #feature-info  This script does an automatic hubble palette simulation for RGB images captured using dual narrowband filter

   var script_title = "Dual NB to Hubble palette"
   var script_description = "This script does an automatic hubble palette simulation for RGB images captured using dual narrowband filter"

   #include <pjsr/UndoFlag.jsh>
   #include <pjsr/NumericControl.jsh>
   #include <pjsr/Sizer.jsh>
   #include <pjsr/ImageOp.jsh>

   #include "utils.jsh"

   // define a global variable containing script's parameters
   var ScriptParameters = {
      myParameter: 0,
      targetView: undefined,

      // stores the current parameters values into the script instance
      save: function() {
         Parameters.set("myParameter", ScriptParameters.myParameter);
      },

      // loads the script instance parameters
      load: function() {
         if (Parameters.has("myParameter"))
            ScriptParameters.myParameter = Parameters.getReal("myParameter")
      }
   }

   /*
    * Construct the script dialog interface
    */
   function DualNBRGBtoHubblePaletteDialog() {
      this.__base__ = Dialog;
      this.__base__();

      // let the dialog to be resizable by fragging its borders
      this.userResizable = true;

      // set the minimum width and height of the dialog
      this.scaledMinWidth = 350;

      // create a title area
      // 1. sets the formatted text
      // 2. sets read only, we don't want to modify it
      // 3. sets the background color
      // 4. sets a fixed height, the control can't expand or contract
      this.title = new TextBox(this);
      this.title.text = "<b>" + script_title + "</b>" +
                        "<br><br>" + script_description;
      this.title.readOnly = true;
      this.title.backroundColor = 0x333333ff;
      this.title.minHeight = 150;
      this.title.maxHeight = 150;

      // add a view picker
      // 1. retrieve the whole view list (images and previews)
      // 2. sets the initially selected view
      // 3. sets the selection callback: the target view becomes the selected view
      this.viewList = new ViewList(this);
      this.viewList.getAll();
      ScriptParameters.targetView = this.viewList.currentView;
      this.viewList.onViewSelected = function (view) {
         ScriptParameters.targetView = view;
      }

/*
      // create the input slider
      // 1. sets the text
      // 2. stes a fixed label width
      // 3. sets the range of the value
      // 4. sets the value precision (number of decimal digits)
      // 5. sets the range of the slider
      // 6. sets a tooltip text
      // 7. defines the behaviour on value change
      this.myParameterControl = new NumericControl(this);
      this.myParameterControl.label.text = "Saturation level:";
      this.myParameterControl.label.width = 60;
      this.myParameterControl.setRange(0, 1);
      this.myParameterControl.setPrecision( 2 );
      this.myParameterControl.slider.setRange( 0, 100 );
      this.myParameterControl.toolTip = "<p>Sets the amount of saturation.</p>";
      this.myParameterControl.onValueUpdated = function( value )
      {
         ScriptParameters.myParameter = value;
      };
*/

      // prepare the execution button
      // 1. sets the text
      // 2. sets a fixed width
      // 3. sets the onClick function
      this.execButton = new PushButton(this);
      this.execButton.text = "Execute";
      this.execButton.width = 40;
      this.execButton.onClick = () => {
         this.ok();
      };

      // Add create instance button
      this.newInstanceButton = new ToolButton( this );
      this.newInstanceButton.icon = this.scaledResource( ":/process-interface/new-instance.png" );
      this.newInstanceButton.setScaledFixedSize( 24, 24 );
      this.newInstanceButton.toolTip = "New Instance";
      this.newInstanceButton.onMousePress = () => {
         // stores the parameters
         ScriptParameters.save();
         // create the script instance
         this.newInstance();
      };


      // create a horizontal slider to layout the execution button
      // 1. sets the margins
      // 2. add the newInstanceButton, a spcer and the execButton
      this.execButtonSizer = new HorizontalSizer();
      this.execButtonSizer.margin = 8;
      this.execButtonSizer.add(this.newInstanceButton)
      this.execButtonSizer.addStretch();
      this.execButtonSizer.add(this.execButton)

      // layout the dialog
      this.sizer = new VerticalSizer();
      this.sizer.margin = 8;
      this.sizer.add(this.title);
      this.sizer.addSpacing(8);
      this.sizer.add(this.viewList);
      // this.sizer.addSpacing(8);
      // this.sizer.add(this.myParameterControl);
      this.sizer.addSpacing(8);
      this.sizer.add(this.execButtonSizer);
      this.sizer.addStretch();
   }

   DualNBRGBtoHubblePaletteDialog.prototype = new Dialog;

   function rgb_to_hubble_palette(view) {

      var newWinView = create_window_for_image(view.image, view.id + "_hubble", true);

      var channel_views = extract_rgb_channels(view);

      var r_id = channel_views[0].id;
      var g_id = channel_views[1].id;
      var b_id = channel_views[2].id;

      var oiii_expr = g_id + "*0.7 + " + b_id + "*0.3";

      var r_expr = r_id;
      var g_expr = r_id + "*0.7 + (" + oiii_expr + ")*0.3";
      var b_expr = oiii_expr;

      newWinView.beginProcess(UndoFlag_NoSwapFile);

      // Mix channels using pixel math
      do_multiexpression_pixel_math(
         newWinView,
         r_expr,
         g_expr,
         b_expr
      )

      // Execute SCNR
      do_scnr(newWinView);

      var b_view = View.viewById(b_id);

      // Apply the blue channel as mask
      newWinView.window.mask = b_view.window;

      // Do curves transformation to increment blue intensity
      do_curves_transform(newWinView, {
         "B": 0.5,
      })

      newWinView.endProcess();

      // Close all intermediate image windows
      ImageWindow.windowById(r_id).forceClose();
      ImageWindow.windowById(g_id).forceClose();
      ImageWindow.windowById(b_id).forceClose();
   }

   function main() {

      Console.hide();

      // perform the script on the target view
      if (Parameters.isViewTarget) {
         // load parameters
         ScriptParameters.load();
         Console.show();
         rgb_to_hubble_palette(ScriptParameters.targetView);
         // var oiii_view = build_oiii_channel(g_view, b_view);
         return;
      }

      // is script started from an instance in global context?
      if (Parameters.isGlobalTarget) {
         // load the parameters from the instance
         ScriptParameters.load();
      }

      // direct contect, create and show the dialog
      let dialog = new DualNBRGBtoHubblePaletteDialog;
      dialog.execute();

      // check if a valid target view has been selected
      if (ScriptParameters.targetView && ScriptParameters.targetView.id) {
         Console.show();
         rgb_to_hubble_palette(ScriptParameters.targetView);
         return;
      } else {
         Console.warningln("No target view is specified ");
      }
   }

   main();
